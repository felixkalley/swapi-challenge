# SWAPI-Challenge

## Description
Just playing around with SwiftUI and SWAPI - The Star Wars API

## Screenshots
![image info](Images/1.png){width=30% height=30%}
![image info](Images/2.png){width=30% height=30%}
![image info](Images/3.png){width=30% height=30%}

## Installation
Just clone the project and open in Xcode

## Acknowledgments
SWAPI can be found here: 
https://swapi.dev

Fonts can be found here:
Star Jedi: https://www.dafont.com/de/star-jedi.font
Soloist: https://www.dafont.com/de/soloist.font

