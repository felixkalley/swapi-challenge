//
//  Fonts.swift
//  SWAPI-Challenge
//
//  Created by Felix Kalley on 05.06.24.
//

import Foundation
import SwiftUI

extension Font {
    static func jedi(_ size: Double) -> Font {
        Font.custom("Starjedi", size: size)
    }
    
    static func solo(_ size: Double) -> Font {
        Font.custom("soloist", size: size)
    }
}
