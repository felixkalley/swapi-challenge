//
//  ListView.swift
//  SWAPI-Challenge
//
//  Created by Felix Kalley on 05.06.24.
//

import SwiftUI

struct ListView: View {
    let category: String
    
    @StateObject private var viewModel = ListViewModel()
    
    var body: some View {
        NavigationStack {
            VStack {
                List(viewModel.items, id: \.id) { item in
                    NavigationLink (destination: DetailView(url: item.url, category: category)) {
                        Text(item.displayName)
                            .font(.title3.bold())
                            .foregroundStyle(.yellow)
                    }
                }
                .listStyle(.plain)
                .onAppear {
                    //TODO: Find a better solution for this
                    if !viewModel.hasLoaded {
                        viewModel.fetchData(from: viewModel.urlForCategory(category))
                        viewModel.hasLoaded = true
                    }
                }
                .background(
                    StarsBGView(starCount: 3000)
                )
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        Text(category)
                            .font(.solo(30))
                            .foregroundStyle(.yellow)
                    }
                }
            }
        }
    }
}

#Preview {
    ListView(category: "People")
        .preferredColorScheme(.dark)
}

