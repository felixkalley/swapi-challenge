//
//  DetailView.swift
//  SWAPI-Challenge
//
//  Created by Felix Kalley on 05.06.24.
//

import SwiftUI

struct DetailView: View {
    let url: String
    let category: String
    
    @StateObject private var viewModel = DetailViewModel()
    
    //TODO: Make UI look better
    var body: some View {
        VStack {
            if let item = viewModel.item {
                VStack(alignment: .leading) {
                    Text(item.displayName)
                        .font(.solo(30))
                        .foregroundStyle(.yellow)
                        .padding(.vertical)
                    switch category {
                    case "People": 
                        PeopleView(item: item)
                    case "Films": 
                        FilmView(item: item)
                        //TODO: Implement cases for other categories
                    default: 
                        EmptyView()
                    }
                    //TODO: Add all the missing info here
                    Text("So much additional info here will be added here in the future...")
                        .padding(.vertical)
                    Spacer()
                }
            } else {
                Text("Loading...")
            }
        }
        .padding()
        .onAppear {
            viewModel.fetchDetailData(from: url)
        }
    }
}

#Preview {
    DetailView(url: SWAPIItem.example.url, category: "People")
        .preferredColorScheme(.dark)
}
