//
//  PeopleView.swift
//  SWAPI-Challenge
//
//  Created by Felix Kalley on 07.06.24.
//

import SwiftUI

struct PeopleView: View {
    let item: SWAPIItem
    var body: some View {
        VStack(alignment: .leading) {
            Text("gender: \(item.gender ?? "N/A")")
            Text("born: \(item.birthYear ?? "N/A")")
            Text("heigth: \(item.height ?? "N/A") cm")
            Text("mass: \(item.mass ?? "N/A") kg")
            Text("hair color: \(item.hairColor ?? "N/A")")
            Text("eye color: \(item.eyeColor ?? "N/A")")
            Text("skin color: \(item.skinColor ?? "N/A")")
            //Text("home: \(item.homeworld ?? "N/A")")
            
            //TODO: Add linking for home and arrays
        }
    }
}

#Preview {
    PeopleView(item: SWAPIItem.example)
}
