//
//  FilmView.swift
//  SWAPI-Challenge
//
//  Created by Felix Kalley on 07.06.24.
//

import SwiftUI

struct FilmView: View {
    let item: SWAPIItem
    var body: some View {
        VStack(alignment: .leading) {
            if let id = item.episodeID {
                Text("episode: \(item.episodeID ?? 0)")
            }
            Text(
                """
                opening:
                \(item.openingCrawl ?? "N/A")
                """
            )
            .padding(.horizontal)
            Text("director: \(item.director ?? "N/A")")
            Text("producer: \(item.producer ?? "N/A")")
            Text("releaseDate: \(item.releaseDate ?? "N/A")")
            
            //TODO: Add linking for arrays
        }
    }
}

#Preview {
    PeopleView(item: SWAPIItem.example)
}
