//
//  StarsBGView.swift
//  SWAPI-Challenge
//
//  Created by Felix Kalley on 05.06.24.
//

import SwiftUI

struct StarsBGView: View {
    var starCount: Int
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                VStack {
                    ForEach(0..<starCount, id: \.self) { _ in
                        Circle()
                            .frame(width: 1, height: 1)
                            .position(
                                x: CGFloat.random(in: 0...geometry.size.width),
                                y: CGFloat.random(in: 0...geometry.size.height)
                            )
                            .foregroundColor(.white)
                    }
                }
            }
        }
    }
}


#Preview {
    StarsBGView(starCount: 3000)
        .preferredColorScheme(/*@START_MENU_TOKEN@*/.dark/*@END_MENU_TOKEN@*/)
}
