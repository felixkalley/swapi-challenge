//
//  ContentView.swift
//  SWAPI-Challenge
//
//  Created by Felix Kalley on 05.06.24.
//

import SwiftUI

struct ContentView: View {
    
    //TODO: Maybe put categories and icons in a ViewModel. Also add NavigationPath when implementing linking form data arrays
    
    let categories: [String] = [
        "Films",
        "People",
        "Planets",
        "Species",
        "Starships",
        "vehicles" //lowercase because of font...
    ]
    
    let categoryIcons: [String] = [
        "🎬", "🧑‍🤝‍🧑", "🪐", "👽", "🚀", "🛞"
    ]
    
    var body: some View {
        NavigationStack {
            VStack (alignment: .leading) {
                Text("Dig deep into data from the Star Wars franchise. Choose a category from below:")
                    .padding(.leading)
                    .font(.title3.bold())
                    .foregroundColor(.yellow)
                List {
                    ForEach(categories.indices, id: \.self) { index in
                        NavigationLink(destination: ListView(category: categories[index])) {
                            HStack (spacing: 15) {
                                Text(categoryIcons[index])
                                    .font(.title)
                                Text(categories[index])
                                    .font(.jedi(25))
                                    .foregroundStyle(.yellow)
                            }
                        }
                    }
                    .listRowBackground(Color(red: 0, green: 0, blue: 0, opacity: 0.6))
                }
                .listStyle(.plain)
                .scrollDisabled(true)
                .padding(.top)
            }
            .background(
                StarsBGView(starCount: 3000)
            )
            .toolbar {
                ToolbarItem(placement: .principal) {
                    Text("Swapi ")
                        .font(.solo(50))
                        .foregroundStyle(.yellow)
                }
            }
        }
        .padding()
        .preferredColorScheme(.dark)
        .ignoresSafeArea()
    }
}

#Preview {
    ContentView()
        .preferredColorScheme(.dark)
}
