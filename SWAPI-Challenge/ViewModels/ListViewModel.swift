//
//  ListViewModel.swift
//  SWAPI-Challenge
//
//  Created by Felix Kalley on 06.06.24.
//

import Foundation

class ListViewModel: ObservableObject {
    @Published var items: [SWAPIItem] = []
    @Published var hasLoaded = false
    
    func urlForCategory(_ category: String) -> String? {
        switch category {
        case "People":
            return "https://swapi.dev/api/people/"
        case "Films":
            return "https://swapi.dev/api/films/"
        case "Planets":
            return "https://swapi.dev/api/planets/"
        case "Species":
            return "https://swapi.dev/api/species/"
        case "Starships":
            return "https://swapi.dev/api/starships/"
        case "vehicles": // lowercase because of font...
            return "https://swapi.dev/api/vehicles/"
        default:
            return nil
        }
    }
    
    //TODO: Make method generic and put into some network helper?
    func fetchData(from urlString: String?) {
        guard let urlString = urlString, let url = URL(string: urlString) else {
            print("Invalid URL")
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                print("No data in response: \(error?.localizedDescription ?? "Unknown error")")
                return
            }
            
            do {
                let decodedResponse = try JSONDecoder().decode(SWAPIResponse.self, from: data)
                DispatchQueue.main.async {
                    self.items.append(contentsOf: decodedResponse.results)
                }
                if let next = decodedResponse.next {
                    self.fetchData(from: next)
                }
            } catch {
                print("Failed to decode response data: \(error)")
                if let json = try? JSONSerialization.jsonObject(with: data, options: []) {
                    print("Received JSON: \(json)")
                } else {
                    print("Failed to parse JSON data")
                }
            }
        }.resume()
    }
}
