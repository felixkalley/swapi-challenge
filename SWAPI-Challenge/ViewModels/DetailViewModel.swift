//
//  DetailViewModel.swift
//  SWAPI-Challenge
//
//  Created by Felix Kalley on 06.06.24.
//

import Foundation

class DetailViewModel: ObservableObject {
    
    @Published var item: SWAPIItem?
    
    //TODO: Add relevant things for DetailView data here
    
    //TODO: Make method generic and put into some network helper?
    func fetchDetailData(from urlString: String) {
        guard let url = URL(string: urlString) else {
            print("Invalid URL")
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                print("No data in response: \(error?.localizedDescription ?? "Unknown error")")
                return
            }
            
            do {
                let decodedResponse = try JSONDecoder().decode(SWAPIItem.self, from: data)
                DispatchQueue.main.async {
                    self.item = decodedResponse
                }
            } catch {
                print("Failed to decode response data: \(error)")
                if let json = try? JSONSerialization.jsonObject(with: data, options: []) {
                    print("Received JSON: \(json)")
                } else {
                    print("Failed to parse JSON data")
                }
            }
        }.resume()
    }
}
