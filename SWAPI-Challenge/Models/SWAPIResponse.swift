//
//  SWAPIResponse.swift
//  SWAPI-Challenge
//
//  Created by Felix Kalley on 05.06.24.
//

import Foundation

struct SWAPIResponse: Codable {
    let count: Int
    let next: String?
    let previous: String?
    let results: [SWAPIItem]
}
