//
//  SWAPIItem.swift
//  SWAPI-Challenge
//
//  Created by Felix Kalley on 05.06.24.
//

import Foundation

struct SWAPIItem: Codable, Identifiable {
    let id = UUID()
    let category = ""
    let name: String?
    let title: String?
    let url: String
    
    // People
    let birthYear: String?
    let eyeColor: String?
    let gender: String?
    let hairColor: String?
    let height: String?
    let mass: String?
    let skinColor: String?
    let homeworld: String?
    
    // Films
    let episodeID: Int?
    let openingCrawl: String?
    let director: String?
    let producer: String?
    let releaseDate: String?
    
    //TODO: Add data from other categories
    
    let films: [String]?
    let species: [String]?
    let vehicles: [String]?
    let starships: [String]?
    let planets: [String]?
    
    let characters: [String]?
    let residents: [String]?
    let people: [String]?
    let pilots: [String]?
    
    var displayName: String {
        return name ?? title ?? "Unknown"
    }
    
    enum CodingKeys: String, CodingKey {
        case name, title, url
        case birthYear = "birth_year"
        case eyeColor = "eye_color"
        case gender, height, mass, homeworld
        case hairColor = "hair_color"
        case skinColor = "skin_color"
        case episodeID = "episode_id"
        case openingCrawl = "opening_crawl"
        case director, producer
        case releaseDate = "release_date"
        case films, species, vehicles, starships, planets, characters, residents, people, pilots
    }
}

extension SWAPIItem {
    static var example: SWAPIItem {
        return SWAPIItem(
            name: "Example Name",
            title: nil,
            url: "https://swapi.dev/api/people/1",
            birthYear: "1980",
            eyeColor: "blue",
            gender: "N/A",
            hairColor: "black",
            height: "1,68m",
            mass: "70kg",
            skinColor: "white",
            homeworld: "Mars",
            episodeID: 1,
            openingCrawl: "In a world...",
            director: "George Lucas",
            producer: "George Lucas",
            releaseDate: "01/01/1984",
            films: ["https://swapi.dev/api/films/1"],
            species: ["https://swapi.dev/api/species/1"],
            vehicles: ["https://swapi.dev/api/vehicles/1"],
            starships: ["https://swapi.dev/api/starships/1"],
            planets: nil,
            characters: nil,
            residents: nil,
            people: nil,
            pilots: nil
        )
    }
}
