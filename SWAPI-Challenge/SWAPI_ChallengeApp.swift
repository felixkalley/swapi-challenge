//
//  SWAPI_ChallengeApp.swift
//  SWAPI-Challenge
//
//  Created by Felix Kalley on 05.06.24.
//

import SwiftUI

@main
struct SWAPI_ChallengeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
